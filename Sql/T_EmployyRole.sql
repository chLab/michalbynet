USE [Bynet]
GO

/****** Object:  Table [dbo].[T_EmployeeRole]    Script Date: 26/06/2022 17:49:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T_EmployeeRole](
	[EmployeeRoleId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeRoleDesc] [nvarchar](100) NULL,
 CONSTRAINT [PK_T_EmployeeType] PRIMARY KEY CLUSTERED 
(
	[EmployeeRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

