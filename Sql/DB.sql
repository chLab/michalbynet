USE [master]
GO
/****** Object:  Database [Bynet]    Script Date: 26/06/2022 17:48:43 ******/
CREATE DATABASE [Bynet]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Bynet', FILENAME = N'C:\Users\hanochg\Bynet.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Bynet_log', FILENAME = N'C:\Users\hanochg\Bynet_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Bynet] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Bynet].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Bynet] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Bynet] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Bynet] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Bynet] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Bynet] SET ARITHABORT OFF 
GO
ALTER DATABASE [Bynet] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Bynet] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Bynet] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Bynet] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Bynet] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Bynet] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Bynet] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Bynet] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Bynet] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Bynet] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Bynet] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Bynet] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Bynet] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Bynet] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Bynet] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Bynet] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Bynet] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Bynet] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Bynet] SET  MULTI_USER 
GO
ALTER DATABASE [Bynet] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Bynet] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Bynet] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Bynet] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Bynet] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Bynet] SET QUERY_STORE = OFF
GO
USE [Bynet]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [Bynet]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 26/06/2022 17:48:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[EmployeeId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[IdentityId] [nvarchar](9) NOT NULL,
	[RoleId] [int] NULL,
	[ManagerId] [int] NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_EmployeeRole]    Script Date: 26/06/2022 17:48:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_EmployeeRole](
	[EmployeeRoleId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeRoleDesc] [nvarchar](100) NULL,
 CONSTRAINT [PK_T_EmployeeType] PRIMARY KEY CLUSTERED 
(
	[EmployeeRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Employee] FOREIGN KEY([ManagerId])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Employee]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_T_EmployeeType] FOREIGN KEY([RoleId])
REFERENCES [dbo].[T_EmployeeRole] ([EmployeeRoleId])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_T_EmployeeType]
GO
USE [master]
GO
ALTER DATABASE [Bynet] SET  READ_WRITE 
GO
