import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { EmployeeService } from '../_core/services/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  // Table

  // Http
  employeeData: any[] = [];
  managerData: any[] = [];
  employeeRole: any[] = [];

  // Form
  employeeForm: FormGroup;
  employeeDataSelected;

  // Modal
  modalRef?: BsModalRef;

  constructor(
    private employeeService: EmployeeService,
    private modalService: BsModalService,
    private fb: FormBuilder,

  ) { }

  ngOnInit(): void {
    this.initForm()
    this.getEmployee();
  }

  // Form
  initForm() {
    this.employeeForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      identityId: ['', Validators.required],
      roleId: [''],
      managerId: [''],
    })
  }

  onSubmit() {
    debugger
    let values = this.employeeForm.value;
    values.roleId  = (values.roleId === '' || values.roleId === null) ? null : values.roleId
    values.managerId  = (values.managerId === '' || values.managerId === 'null') ? null : values.managerId
    this.employeeService.createEmployee(values)
      .subscribe(item => {
        this.getEmployee();
      })
  }

  openModal(template: TemplateRef<any>, row) {
    this.getEmployeeRole();
    console.log(row);
    // Edit
    if (row) {
      this.employeeDataSelected = Object.assign({}, row);
      console.log(this.employeeDataSelected);
      // Set form value
      this.employeeForm.patchValue({
        firstName: this.employeeDataSelected.firstName,
        lastName: this.employeeDataSelected.lastName,
        identityId: this.employeeDataSelected.identityId,
        roleId: this.employeeDataSelected.roleId,
        managerId: this.employeeDataSelected.managerId,
      });
    }
    // Add
    else {
      this.initForm()
      this.employeeDataSelected = null;
    }
    this.modalRef = this.modalService.show(template);
  }

  removeEmployee() {
    this.employeeService.removeEmployee(this.employeeDataSelected.employeeId)
      .subscribe(item => {
        this.getEmployee();
      })
  }


  // Http
  getEmployee() {
    this.employeeService.getAllEmployee()
      .subscribe(item => {
        this.employeeData = item;
      })
  }
  getManager() {
    this.employeeService.getAllManager()
      .subscribe(item => {
        this.managerData = item;
      })
  }
  getEmployeeRole() {
    this.employeeService.getEmployeeRole()
      .subscribe(item => {
        this.employeeRole = item;
      })
  }
}
