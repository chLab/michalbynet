import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { JwtService } from '../services/_jwt.service';
import { tap } from 'rxjs/operators';


@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {
    constructor(private jwtService: JwtService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const headersConfig = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };

        const request = req.clone({ setHeaders: headersConfig });
        return next.handle(request);

        return next.handle(req)
            .pipe(tap(
                (event: HttpEvent<any>) => {
                    if (event instanceof HttpResponse) {
                    }
                }, (error: HttpErrorResponse) => {
                    // if (err instanceof HttpErrorResponse) {
                    //     alert(JSON.stringify(err.message))
                    //     if (err.status === 401) {
                    //         // this.router.navigate(['auth/login']);
                    //     }
                    // }

                    let errorMessage = '';
                    if (error.error instanceof ErrorEvent) {
                        // client-side error
                        errorMessage = `Error: ${error.error.message}`;
                    } else {
                        // server-side error
                        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
                    }
                    debugger
                    window.alert(errorMessage);
                    return throwError(errorMessage);
                }));
    }
}