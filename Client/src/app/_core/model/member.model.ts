export interface Member {
    id?: string,
    identity: string,
    firstName: string,
    lastName: string,
    address: string,
    phone: string,
    cell: string,
}


export interface ActiveLessonInLesson {
    memberId;
    joinDate: string;
    fullName;
}

export interface ActiveMemberNotInLesson {
    memberId: number;
    fullName: string;
}

export interface MemberInLessonCreate {
    memberId;
    lessonId;
}