export interface MemberInLessonResponse {
    lessonId;
    memberId;
    joinDate;
    fullName
}

export interface ActiveMemberForAttandeece {
    fullName,
    programName,
    visitsLeft,
    memberId,
    subscriptionId,
}

