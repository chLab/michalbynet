export class Subscription {
  id = null;
  actionType = null;
  memberId = null;
  programId = null;
  startDate = null;
  endDate  = null;
  visit = null;
  price = null;

  isMeuhedet = null;
  
  confirmationType = null;
  confirmationToSupply = null;
  confirmationToProduce = null;
}


export class SubscriptionUpdate {
  id = null;
  programId = null;
  startDate = null;
  endDate  = null;
  visit = null;
  price = null;
}

export class SubscriptionDetails {
  id = null;
  actionType = null;
  actionDate = null;
  memberId = null;
  programId = null;
  startDate = null;
  endDate = null;
  visit = null;
  price = null;
  comment = null;
  programName = null;
  numberOfVisitPerWeek = null;
  daysLeft = null;
  visited = null;
  visitsLeft = null;
  status = null;
  monthLeft = null;
}
