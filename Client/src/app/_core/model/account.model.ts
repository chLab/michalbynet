export interface AccountResponse {
  id: number;
  actionDate: string;
  memberId: string;
  subscriptionId: number;
  programName: string;
  paymentMethodDesc: string;
  comment: string;
}
