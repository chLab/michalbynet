export interface Instructor {
    id?;
    firstName;
    lastName;
    cell;
    phone;
    mail;
}