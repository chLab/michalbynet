import { Instructor } from './instructor.model';

export interface Lesson {
    id?: string,
    instructorId;
    name;
    day;
    maxMember;
    status;
    code;
}

export interface LessonDetails {
    id;
    instructorFullName;
    name;
    day;
    code;
    maxMember;
    instructorId;
  }