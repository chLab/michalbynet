export class Visit {
    visitId? = null;
    memberId = null;
    subscriptionId = null;
    lessonId = null;
    date: any;
    comment?: string;
  }

  
  export interface VisitDetails {
    visitId: number;
    memberId: number;
    date: string;
    day,
    programName,
    lessonName,
    instructorFullName,
    comment,
    subscriptionId,
    lessonId
  }
  