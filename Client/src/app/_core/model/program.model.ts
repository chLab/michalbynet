export interface Program {
    id?: number;
    name: string,
    numberOfMonth: number,
    visitPerWeek: number,
    price: number,
}

