export const CONFIG = {
    DAYS_OF_WEEK: ['א', 'ב', 'ג', 'ד', 'ה', 'ו', 'מוצ"ש'],
    DAYS_OF_WEEK_Full_NAME: ['ראשון', 'שני', 'שלישי', 'רביעי', 'חמישי', 'שישי', 'שבת', 'מוצ"ש'],
    NUMBER_OF_MONTH: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
    VISIT_IN_WEEK: [1, 2],
}