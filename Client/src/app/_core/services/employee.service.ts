import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './_api.service';
import { Observable } from 'rxjs';
import { User } from '../model/user.model';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class EmployeeService {

    private employeAPI = 'employee';

    constructor(
        private http: ApiService
    ) { }

    getAllEmployee(): Observable<any> {
        return this.http.get(`${this.employeAPI}/worker`)
            .pipe(
                map(item => item.map(e => {
                    e.fullName = e.firstName + ' ' + e.lastName
                    return e;
                }))
            );
    }

    getAllManager(): Observable<any> {
        return this.http.get(`${this.employeAPI}/manager`)
            .pipe(
                map(item => item.map(e => {
                    e.fullName = e.firstName + ' ' + e.lastName
                    return e;
                }))
            );
    }

    createEmployee(data): Observable<any> {
        return this.http.post(`${this.employeAPI}/employee`, data)
    }

    updateEmployee(data): Observable<any> {
        return this.http.put(`${this.employeAPI}`, data)
    }

    getEmployeeRole(): Observable<any> {
        return this.http.get(`${this.employeAPI}/role`)
    }

    removeEmployee(id): Observable<any> {
        return this.http.delete(`${this.employeAPI}/${id}`)
    }
}