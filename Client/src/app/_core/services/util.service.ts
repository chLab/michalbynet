import { Injectable } from '@angular/core';
import { ApiService } from './_api.service';
import { Observable } from 'rxjs';
import { Visit, VisitDetails } from '../model/visit.model';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from '../config';

@Injectable({
    providedIn: 'root',
})
export class UtilService {

    constructor(
        private httpOrigin: HttpClient
    ) { }

    // Set all button icon and style
    setTableButton(setting: any) {
        // Set button
        setting.add = {
            addButtonContent: '<i class="nb-plus"></i>',
            createButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
        }
        setting.edit = {
            editButtonContent: '<i class="nb-edit"></i>',
            saveButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
        }
        setting.delete = {
            deleteButtonContent: '<i class="nb-trash"></i>',
            confirmDelete: true,
        }
        return setting
    }

    getCurrentDate(val: Date): string {
        if (val) {
            return val.toISOString().substring(0, 10);
        } else {
            const currentDate = new Date();
            return currentDate.toISOString().substring(0, 10);
        }
    }

    
    getDateReverse(date: string): string {
        // check if empty
        if (date == null || date == '') return;

        let temp = date.split('-');
        let fixDate = temp[2] + '-' + temp[1] + '-' + temp[0]
        return fixDate;
    }

    convertDateToHebrew(val: string): Observable<any> {
        let path = 'https://www.hebcal.com/converter/';
        let date = new Date(val)
        //?gd=14&gm=9&gy=2018&g2h=1&cfg=json
        let d = date.getDate()
        let m = date.getMonth() + 1;
        let y = date.getFullYear();

        return this.httpOrigin.get(`${path}?gd=${d}&gm=${m}&gy=${y}&g2h=1&cfg=json`);
    }

    getDayFromDate(date: Date): string {
        let day = CONFIG.DAYS_OF_WEEK_Full_NAME[date.getDay()];
        return day;
    }

}

