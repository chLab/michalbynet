import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './_api.service';
import { Observable } from 'rxjs';
import { User } from '../model/user.model';


@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(
        // private http: HttpClien,
        private http: ApiService
    ) { }

    // getAll() {
    //     return this.http.get<User[]>(`${environment.apiUrl}/users`);
    // }

    getAll(): Observable<User> {
        return this.http.get('/users')
    }
}