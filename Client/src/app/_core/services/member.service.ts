import { Injectable } from '@angular/core';
import { ApiService } from './_api.service';
import { Observable } from 'rxjs';

import { ActiveLessonInLesson, Member } from '../model';
import { ActiveMemberForAttandeece, MemberInLessonResponse } from '../model/memberInLesson.model';
import { map } from 'rxjs/operators';
import { UtilService } from './util.service';

@Injectable({
    providedIn: 'root',
})
export class MemberService {

    private memberApi = 'members';

    private currentMember: Member;

    constructor(
        private http: ApiService,
        private utilService: UtilService,
    ) { }

    getAll(): Observable<Member[]> {
        return this.http.get(this.memberApi);
    }

    getActiveMemberByLessonId(lessonId): Observable<ActiveLessonInLesson[]> {
        return this.http.get(`${this.memberApi}/lesson/${lessonId}`);
    }

    getmemberInLesson(lessonId): Observable<MemberInLessonResponse[]> {
        return this.http.get(`membersInLesson/${lessonId}`);
    }

    getmemberInLessonForAttendance(lessonId): Observable<MemberInLessonResponse[]> {
        return this.http.get(`membersInLesson/${lessonId}`)
            .pipe(
                map(item => item.map(e => {
                    e.joinDate = this.utilService.getDateReverse(e.joinDate)
                    return e;
                }))
            );
    }

    getmemberInLessonForAttendance_Temp(lessonId): Observable<ActiveMemberForAttandeece[]> {
        return this.http.get(`members/attendance/lesson/${lessonId}`)
    }

    getById(id): Observable<Member> {
        return this.http.get(`${this.memberApi}/${id}`);
    }

    addLMember(val: Member): Observable<any> {
        return this.http.post(this.memberApi, val);
    }

    updatMember(val: Member): Observable<any> {
        return this.http.put(`${this.memberApi}`, val);
    }


    initCurrMember(value) {
        this.currentMember = value;
    }

    getCurrMember(): Member {
        return this.currentMember;
    }
}
