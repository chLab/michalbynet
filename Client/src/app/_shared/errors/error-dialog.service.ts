import { Injectable, TemplateRef } from "@angular/core";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { ErrorDialogComponent } from "./error-dialog/error-dialog.component";

@Injectable({
    providedIn: 'root'
})
export class ErrorDialogService {

    modalRef: BsModalRef;
    constructor(private modalService: BsModalService) { }

    openModal(message, val) {
        this.modalRef = this.modalService.show(ErrorDialogComponent);
    }
}