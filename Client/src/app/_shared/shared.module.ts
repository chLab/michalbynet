import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorDialogComponent } from './errors/error-dialog/error-dialog.component';
// import { ModalModule } from 'ngx-bootstrap/modal';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// import { TableModule } from 'ngx-easy-table';

const sharedComponents = [ErrorDialogComponent];

const ngxModule = [ModalModule.forRoot()];

@NgModule({
  declarations: [
    ...sharedComponents,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    // TableModule,
    ...ngxModule
  ],
  exports: [
    ...sharedComponents,
    ReactiveFormsModule
    // TableModule,
  ],
})
export class SharedModule { }
