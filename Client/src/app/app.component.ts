import { Component, OnInit } from '@angular/core';
import { ApiService } from './_core/services/_api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  constructor(
    private apiSerice: ApiService
  ) { }

  ngOnInit(): void {
  }
}
