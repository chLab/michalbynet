import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
// import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
// import { Config, Columns, DefaultConfig } from 'ngx-easy-table';
import { EmployeeService } from '../_core/services/employee.service';

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css']
})
export class ManagerComponent implements OnInit {

  // Table
  // configuration: Config;
  // columns: Columns[];

  // Http
  managerData: any[] = [];
  employeeRole: any[] = [];

  // Form
  managerForm: FormGroup;
  managerDataSelected;

  // Modal
  modalRef?: BsModalRef;

  constructor(
    private employeeService: EmployeeService,
    private modalService: BsModalService,
    private fb: FormBuilder,

  ) { }


  ngOnInit(): void {
    this.initTable();
    this.initForm();
    this.getManager();
  }

  // Table
  initTable() {

    // this.columns = [
    //   { key: 'fullName', title: 'שם מלא' },
    //   { key: 'identityId', title: 'ת.ז' },
    //   { key: 'roleDesc', title: 'תפקיד' },
    //   { key: 'isActive', title: 'עריכה' },
    // ];

    // let config = { ...DefaultConfig };
    // config.paginationEnabled = false;
    // this.configuration = config
  }


  // Form
  initForm() {
    this.managerForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      identityId: ['', Validators.required],
      roleId: [null],
    })
  }

  onSubmit() {
    if (this.managerDataSelected) {
      let val = this.managerForm.value;
      val.employeeId = this.managerDataSelected.employeeId;
      this.employeeService.updateEmployee(val)
        .subscribe(item => {
          this.getManager();
        })
    } else {
      this.employeeService.createEmployee(this.managerForm.value)
        .subscribe(item => {
          this.getManager();
        })
    }
  }

  removeEmployee() {
    this.employeeService.removeEmployee(this.managerDataSelected.employeeId)
      .subscribe(item => {
        this.getManager();
      })
  }

  openModal(template: TemplateRef<any>, row) {
    this.getEmployeeRole();

    // Edit
    if (row) {
      this.managerDataSelected = Object.assign({}, row);
      console.log(this.managerDataSelected);
      // Set form value
      debugger
      this.managerForm.patchValue({
        firstName: this.managerDataSelected.firstName,
        lastName: this.managerDataSelected.lastName,
        identityId: this.managerDataSelected.identityId,
        roleId: this.managerDataSelected.roleTypeId,
      });
      this.managerForm.controls['identityId'].disable();
    }
    // Add
    else {
      this.initForm();
      // this.managerForm.controls['identityId'].enable();
      this.managerDataSelected = null;
    }
    this.modalRef = this.modalService.show(template);
  }


  // Http
  getManager() {
    this.employeeService.getAllManager()
      .subscribe(item => {
        this.managerData = item;
      })
  }
  getEmployeeRole() {
    this.employeeService.getEmployeeRole()
      .subscribe(item => {
        this.employeeRole = item;
      })
  }
}
