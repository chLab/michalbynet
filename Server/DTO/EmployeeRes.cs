﻿namespace DTO
{
    public class EmployeeRes
    {
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IdentityId { get; set; }
        public string ManagerName { get; set; }
        public int? ManagerId { get; set; }
        public int? RoleTypeId { get; set; }
        public string RoleTypeDesc { get; set; }
    }
}
