﻿namespace DTO
{
    public class EmployeeAddReq
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IdentityId { get; set; }
        public int? RoleId { get; set; }
        public int? ManagerId { get; set; }
    }
}
