﻿namespace DTO
{
    public class EmployeeUpdateReq
    {
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IdentityId { get; set; }
        public int? RoleId { get; set; }
        public int? ManagerId { get; set; }
    }
}
