﻿using DAL.Interface;
using DTO;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeelRepository _employeelRepository;

        public EmployeeController(IEmployeelRepository employeelRepository)
        {
            _employeelRepository = employeelRepository;
        }

        // GET: api/<ValuesController>type
        [HttpGet("role")]
        public ActionResult<List<CodeTable>> GetEmployeeRole()
        {
            return Ok(_employeelRepository.GetEmployeeRole());
        }

        // GET: api/<ValuesController>worker
        [HttpGet("worker")]
        public ActionResult<List<EmployeeRes>> GetWorker()
        {
            return Ok(_employeelRepository.GetWorker());
        }

        // GET api/<ValuesController>/manager
        [HttpGet("manager")]
        public ActionResult<List<EmployeeRes>> GetManager()
        {
            return Ok(_employeelRepository.GetManager());
        }

        // POST api/<ValuesController>/worker
        [HttpPost("employee")]
        public void PostWorker([FromBody] EmployeeAddReq value)
        {
            _employeelRepository.AddEmployee(value);
        }

        // PUT api/<ValuesController>/5
        [HttpPut]
        public void PutEmployee([FromBody] EmployeeUpdateReq value)
        {
            _employeelRepository.UpdateEmployee(value);

        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _employeelRepository.DeleteWorker(id);
        }
    }
}
