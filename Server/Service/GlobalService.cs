﻿using Microsoft.Extensions.Configuration;
using Service.Interface;

namespace Service
{
    public class GlobalService : IGlobalService
    {
        private readonly IConfiguration _configuration;
        public GlobalService(IConfiguration configuration)
        {
            _configuration = configuration;
        }


        public byte[] GetHelpFile()
        {
            var helpFilePath = _configuration["AppSettings:HelpFilePath"];

            return System.IO.File.ReadAllBytes(helpFilePath);
        }
    }
}
