﻿using Microsoft.Extensions.DependencyInjection;
using Service;
using Service.Interface;

namespace DAL
{
    public static class Register_ServiceCollection
    {
        public static void AddServices(this IServiceCollection services)
        {
            // Services
            services.AddScoped<IGlobalService, GlobalService>();
        }
    }
}
