﻿namespace Service.Helper
{
    public class ConfigUserGroup
    {
        public string MngGroup { get; set; }
        public string MngReportGroup { get; set; }
        public string HelpDeskGroup { get; set; }
    }
}
