﻿using DAL.Context;
using DAL.Interface;
using DTO;
using System.Collections.Generic;
using System.Linq;

namespace DAL
{
    public class EmployeeRepository : IEmployeelRepository
    {
        private readonly BynetContext _context;
        public EmployeeRepository(BynetContext BynetContext)
        {
            _context = BynetContext;
        }

        public List<EmployeeRes> GetWorker()
        {
            var res = _context.Employees.Select(x => new EmployeeRes
            {
                EmployeeId = x.EmployeeId,
                FirstName = x.FirstName,
                LastName = x.LastName,
                RoleTypeId = x.RoleId,
                RoleTypeDesc = x.Role.EmployeRoleDesc,
                IdentityId = x.IdentityId,
                ManagerId = x.ManagerId ?? null,
                ManagerName = x.ManagerId.HasValue ? _context.Employees.Where(y => y.EmployeeId == x.ManagerId).Select(x => x.FirstName + " " + x.LastName).First() : ""
            }).ToList();

            return res;
        }

        public EmployeeRes GetWorker(int id)
        {
            var res = _context.Employees.Where(x => x.EmployeeId == id).Select(x => new EmployeeRes
            {
                EmployeeId = x.EmployeeId,
                FirstName = x.FirstName,
                LastName = x.LastName,
                RoleTypeId = x.RoleId,
                RoleTypeDesc = x.Role.EmployeRoleDesc,
                IdentityId = x.IdentityId,
                ManagerId = x.ManagerId ?? null,
                ManagerName = x.ManagerId.HasValue ? _context.Employees.Where(y => y.EmployeeId == x.ManagerId).Select(x => x.FirstName + " " + x.LastName).First() : ""
            }).First();

            return res;
        }

        public List<EmployeeRes> GetManager()
        {
            var res = _context.Employees.Where(x => x.ManagerId == null).Select(x => new EmployeeRes
            {
                EmployeeId = x.EmployeeId,
                FirstName = x.FirstName,
                LastName = x.LastName,
                RoleTypeId = x.RoleId,
                RoleTypeDesc = x.Role.EmployeRoleDesc,
                IdentityId = x.IdentityId,
                ManagerId = x.ManagerId ?? null,
                ManagerName = x.ManagerId.HasValue ? _context.Employees.Where(y => y.EmployeeId == x.ManagerId).Select(x => x.FirstName + " " + x.LastName).First() : ""
            }).ToList();

            return res;
        }

        public EmployeeRes GetManager(int id)
        {
            throw new System.NotImplementedException();
        }

        public void UpdateEmployee(EmployeeUpdateReq req)
        {
            var entity = _context.Employees.Where(x => x.EmployeeId == req.EmployeeId).First();

            entity.FirstName = req.FirstName;
            entity.LastName = req.LastName;
            entity.RoleId = req.RoleId ?? null;
            entity.ManagerId = req.ManagerId;

            _context.SaveChanges();
        }

        public void AddEmployee(EmployeeAddReq req)
        {
            _context.Employees.Add(new Entities.Employee
            {
                FirstName = req.FirstName,
                LastName = req.LastName,
                IdentityId = req.IdentityId,
                RoleId = req.RoleId ?? null,
                ManagerId = req.ManagerId ?? null,
            });
            _context.SaveChanges();
        }

        public void DeleteWorker(int id)
        {
            // Get all worker of this manager
            var employee = _context.Employees.Where(x => x.ManagerId == id).ToList();
            foreach (var item in employee)
            {
                item.ManagerId = null;
            }

            _context.SaveChanges();

            var entity = _context.Employees.Where(x => x.EmployeeId == id).First();
            _context.Employees.Remove(entity);
            _context.SaveChanges();
            //try
            //{
            //    _context.SaveChanges();
            //}
            //catch (System.Exception ex)
            //{

            //    throw;
            //}
        }

        public List<CodeTable> GetEmployeeRole()
        {
            return _context.TEmployeeRoles.Select(x => new CodeTable
            {
                Id = x.EmployeeRoleId,
                Desc = x.EmployeRoleDesc
            }).ToList();
        }
    }
}
