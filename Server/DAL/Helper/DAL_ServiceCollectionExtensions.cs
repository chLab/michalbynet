﻿using DAL.Interface;
using Microsoft.Extensions.DependencyInjection;

namespace DAL.Helper
{
    public static class DAL_ServiceCollectionExtensions
    {
        public static void AddDal(this IServiceCollection services)
        {
            //services.AddScoped<IDapperBaseRepository, DapperBaseRepository>();

            // Repository
            services.AddScoped<IEmployeelRepository, EmployeeRepository>();
        }
    }
}
