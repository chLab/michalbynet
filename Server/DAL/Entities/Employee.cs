﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Entities
{
    public partial class Employee
    {
        public Employee()
        {
            InverseManager = new HashSet<Employee>();
        }

        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IdentityId { get; set; }
        public int? RoleId { get; set; }
        public int? ManagerId { get; set; }

        public virtual Employee Manager { get; set; }
        public virtual TEmployeeRole Role { get; set; }
        public virtual ICollection<Employee> InverseManager { get; set; }
    }
}
