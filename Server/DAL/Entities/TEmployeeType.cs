﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Entities
{
    public partial class TEmployeeType
    {
        public TEmployeeType()
        {
            Employees = new HashSet<Employee>();
        }

        public int EmployeeTypeId { get; set; }
        public string EmployeTypeDesc { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
    }
}
