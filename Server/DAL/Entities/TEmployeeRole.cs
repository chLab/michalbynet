﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Entities
{
    public partial class TEmployeeRole
    {
        public TEmployeeRole()
        {
            Employees = new HashSet<Employee>();
        }

        public int EmployeeRoleId { get; set; }
        public string EmployeRoleDesc { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
    }
}
