﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using DAL.Entities;

#nullable disable

namespace DAL.Context
{
    public partial class BynetContext : DbContext
    {
        public BynetContext()
        {
        }

        public BynetContext(DbContextOptions<BynetContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<TEmployeeRole> TEmployeeRoles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>(entity =>
            {
                entity.ToTable("Employee");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.IdentityId)
                    .IsRequired()
                    .HasMaxLength(9);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.Manager)
                    .WithMany(p => p.InverseManager)
                    .HasForeignKey(d => d.ManagerId)
                    .HasConstraintName("FK_Employee_Employee");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_Employee_T_EmployeeType");
            });

            modelBuilder.Entity<TEmployeeRole>(entity =>
            {
                entity.HasKey(e => e.EmployeeRoleId)
                    .HasName("PK_T_EmployeeType");

                entity.ToTable("T_EmployeeRole");

                entity.Property(e => e.EmployeRoleDesc).HasMaxLength(100);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
