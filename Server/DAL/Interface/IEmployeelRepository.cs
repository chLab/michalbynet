﻿using DTO;
using System.Collections.Generic;

namespace DAL.Interface
{
    public interface IEmployeelRepository
    {
        List<CodeTable> GetEmployeeRole();
        List<EmployeeRes> GetWorker();
        EmployeeRes GetWorker(int id);
        List<EmployeeRes> GetManager();
        EmployeeRes GetManager(int id);
        void AddEmployee(EmployeeAddReq req);
        void UpdateEmployee(EmployeeUpdateReq req);
        void DeleteWorker(int id);
    }
}
